import React from 'react';
import {Link} from 'react-router';

import ImagesUploader from 'react-images-uploader';
import 'react-images-uploader/styles.css';
import 'react-images-uploader/font.css';
import Modal from './Modal.js';


import {HTTP_SERVER_PORT_IMAGES} from '../server/constants';

export default class Activities extends React.Component{
    constructor(props) {
        super(props);
        this.state = {activity: null, isOpen:false
        };
        console.log(1);
        this.loadData();

        this.toggle = this.toggle.bind(this);
    }

    loadData() {

        fetch('/activities/' + this.props.params.id)                      // Ask the route /cities to the server
            .then(res => res.json())                       // Retrieve the objects  in json
            .then(data => this.setState({activity: data}))   // Modify the state accordingly
            .catch(err => console.log(err));               // Bad news: an error!

        console.log(this.state.activity);
    }

    toggle() {
        this.setState({isOpen: !this.state.isOpen});
    }

    addComment(e) {
        e.preventDefault();
        const idActivity = this.props.params.id;
        console.log('actId', idActivity)
        const text = e.target.elements["text"].value;

        console.log('postComment ',text)

        fetch('/newComment', {
            method: 'POST', headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({idActivity, text})
        }).then(res => {
            this.loadData();
            if (res.ok) {
                res.json().then(id => console.log("Comment added with id " + id));
            }
            else
                res.json().then(err => alert("Failed to add Comment: " + err.message));
        }).catch(err => alert("Error in sending data to server: " + err.message));

        this.toggle();
    }

    render() {
        if (this.state.activity === null)
            return null;

        return(<div>
            <h2>Place : {this.state.activity.name}</h2>
            <img src={this.state.activity.picture}/>
            {this.state.activity.pictures.map(img => {
                return ( <img src={img} width={"300"} height={"400"}/>
                )
            })
            }
            <p>{this.state.activity.description}</p>
            <p>Editor: {this.state.activity.editor}</p>
            <h2>Comments</h2>
            {this.state.activity.comments.map(cm => {
                return (
                    <div>
                        <p>{cm.user != null ?cm.user.email : 'anonymous'} : {cm.text}</p>
                    </div>
                )
            })
            }
            <p className="addComment"><a id="open" className="btn btn-success" onClick={(e) => this.toggle(e)}>Add
                a comment?</a></p>
            <Modal isOpen={this.state.isOpen} toggle={this.toggle}>
                <form onSubmit={(e) => this.addComment(e)}>
                    <input id="text" name="text" type="text" placeholder="text"/>
                    <button type="submit" className="btn btn-success btn-lg">Create</button> <button className="btn btn-danger btn-lg" onClick={(e) => this.toggle(e)}>Cancel</button>
                </form>
            </Modal>
        </div>)
    }
}
