import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, hashHistory} from 'react-router';

import Home from './Home.js';
import City from './City.js';
import Activities from './Activities.js';
import Modal from './Modal.js';



ReactDOM.render(
    <div>
        <header className="headerbg">

            <div className="desc">
                <img src="img/logo.png" alt="" width="400px"/>
                    <h3>Discover the cities all around the world</h3>
            </div>

            <div className="scroll-downs">
                <div className="mousey">
                    <div className="scroller"></div>
                </div>
            </div>
        </header>

        <div className="container content">
                <Router history={hashHistory}>
                    <Route path="/" component={Home}/>
                    <Route path="/city/:id" component={City} />
                    <Route path="/activities/:id" component={Activities} />
                    <Route path="*" component={() =>
                        <center><h1 className="notfound">404</h1><br /><h3>Page not found</h3></center>
                    }/>
                </Router>
        </div>
        <footer>
            <p>Copyright &copy; 2018 - VelociREACTOR</p>
        </footer>
    </div>
    ,
    document.getElementById('root')

);

if (module.hot)
    module.hot.accept();

