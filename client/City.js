import React from 'react';
import {Link} from 'react-router';

import ImagesUploader from 'react-images-uploader';
import 'react-images-uploader/styles.css';
import 'react-images-uploader/font.css';
import Modal from './Modal.js';
import Activites from './Activities.js';

import {HTTP_SERVER_PORT_IMAGES} from '../server/constants';

class Activity extends React.Component {
    render() {
        return (
            <Link to={'/activities/' + this.props.activity._id} className="col-md-4">
                <div className="cardcity">
                    <div className="imgdivmin">
                    <img className="card-img-top2" src={this.props.activity.picture}/>
                    </div>
                    <div className="card-block">
                        <h5 className="text-bold">{this.props.activity.name}</h5>
                    </div>
                </div>
            </Link>
        );
    }
}

export default class City extends React.Component {
    constructor(props) {
        console.log('Heel');
        super(props);
        this.state = {
            city: null,
            isOpen : false

        };
        this.loadData();
        this.toggle = this.toggle.bind(this);
    }

    loadData() {

        fetch('/cities/' + this.props.params.id)                      // Ask the route /cities to the server
            .then(res => res.json())                       // Retrieve the objects  in json
            .then(data => this.setState({city: data}))   // Modify the state accordingly
            .catch(err => console.log(err));               // Bad news: an error!

        console.log(this.state.city);
    }

    toggle() {
        this.setState({isOpen: !this.state.isOpen});
    }

    addActivity(e) {
        e.preventDefault();
        const idCity = this.props.params.id;
        console.log("Mess" , idCity)
        const activityName = e.target.elements["activityName"].value;
        const activityNature = e.target.elements["activityNature"].value;
        const activityEditor = e.target.elements["activityEditor"].value;
        const activityDescription = e.target.elements["activityDescription"].value;
        const activityPicture = e.target.elements["activityPicture"].value;

        fetch('/newActivity', {
            method: 'POST', headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({idCity, activityName, activityNature, activityEditor, activityDescription, activityPicture})
        }).then(res => {
            if (res.ok) {
                res.json().then(id => console.log("Activity added with id " + id));
                this.loadData();
            }
            else
                res.json().then(err => alert("Failed to add activity: " + err.message));
        }).catch(err => alert("Error in sending data to server: " + err.message));

        this.toggle();
    }

    render() {
        console.log(this.state.city);

        if (this.state.city === null)
            return null;

        return (
            <div className="city-description">
                <div>
                    <div className="desccity">
                        <h1 className="col-md-12">City's name : {this.state.city.name}</h1>
                        <p className="col-md-12 descp">{this.state.city.description}</p>
                    </div>
                    <div className="col-md-12 imgdesc">
                        <center><div className="overlay"></div></center>
                        <center><img src={this.state.city.picture} alt="city's image" className="imgcity"/></center>
                    </div>

                </div>
                <hr/>


                <p className="addActivity"><a id="open" className="btn btn-success" onClick={(e) => this.toggle(e)}>Add
                    an activity?</a></p>
                <Modal isOpen={this.state.isOpen} toggle={this.toggle}>
                    <div className="col-md-3"></div>
                    <form onSubmit={(e) => this.addActivity(e)} className="form-group col-md-6">
                        <h3>Add an activity</h3>

                        <input required id="activityName" className="form-control" name="activityName" type="text" placeholder="Name"/>
                        <input id="activityName" className="form-control" name="activityName" type="text" placeholder="Name"/>

                        <input required id="activityNature" className="form-control" name="activityNature" type="radio" value="place"/>Place
                        <input required id="activityNature" className="form-control" name="activityNature" type="radio" value="event"/>Event

                        <input required id="activityEditor" className="form-control" name="activityEditor" type="text" placeholder="Editor"/>

                        <input required id="activityDescription" className="form-control" name="activityDescription" type="text"
                               placeholder="Description"/>

                        <input required id="activityPicture" type="hidden" value=""/>
                        <ImagesUploader
                            label="Upload a Picture for the activity"
                            url={HTTP_SERVER_PORT_IMAGES} optimisticPreviews multiple={false}
                            onLoadEnd={(err, pictureFileName) => {
                                if(err) console.error(err);
                                else document.getElementById("activityPicture").value = pictureFileName;
                            }}
                        />

                        <button type="submit" className="btn btn-success btn-lg">Create</button> <button className="btn btn-danger btn-lg" onClick={(e) => this.toggle(e)}>Cancel</button>
                    </form>
                    <div className="col-md-3"></div>
                </Modal>
                <div className="">
                    <h2 className="col-md-4">Events :</h2>
                    <div className="row flex-row">

                        {this.state.city.activities.filter(m => m.nature === "event").map((m, i) => <Activity key={i} activity={m}/>)}

                    </div>
                </div>
                <div >
                    <h2 className="col-md-4">Places :</h2>
                    <div className="row flex-row">
                    {this.state.city.activities.filter(m => m.nature === "place").map((m, i) => <Activity key={i} activity={m}/>)}
                   </div>

                </div>

            </div>
        );
    }
}
