import React from 'react';
import {Link} from 'react-router';

import ImagesUploader from 'react-images-uploader';
import 'react-images-uploader/styles.css';
import 'react-images-uploader/font.css';
import Modal from './Modal.js';
import {HTTP_SERVER_PORT_IMAGES} from '../server/constants';

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {cities : [], isOpen : false};
        this.loadData();
        console.log(this.state.cities);

        this.toggle = this.toggle.bind(this);
    }

    loadData() {
        fetch('/cities')                       // Ask the route /cities to the server
            .then(res => res.json())                       // Retrieve the objects  in json
            .then(data => this.setState({cities: data}))   // Modify the state accordingly
            .catch(err => console.log(err));               // Bad news: an error!

        console.log(this.state.city);
    }

    toggle() {
        this.setState({isOpen: !this.state.isOpen});
    }

    addCity(e){
        e.preventDefault();
        const cityName = e.target.elements["cityName"].value;
        const cityLongitude = e.target.elements["cityLongitude"].value;
        const cityLatitude = e.target.elements["cityLatitude"].value;
        const cityPicture = e.target.elements["cityPicture"].value;

        fetch('/newCity', {
            method: 'POST', headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({cityName, cityLongitude, cityLatitude, cityPicture})
        }).then(res => {
            if(res.ok){
                res.json().then(id => console.log("City added with id " + id));
                this.loadData();
            }
            else
                res.json().then(err => alert("Failed to add city: " + err.message));
        }).catch(err => alert("Error in sending data to server: " + err.message));

        this.toggle();
    }

    deleteCity(e){
        e.preventDefault();
        const cityName = e.target.elements["cityName"].value;
        const cityLongitude = e.target.elements["cityLongitude"].value;
        const cityLatitude = e.target.elements["cityLatitude"].value;
        const cityPicture = e.target.elements["cityPicture"].value;

        fetch('/deleteCity', {
            method: 'POST', headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({cityName, cityLongitude, cityLatitude, cityPicture})
        }).then(res => {
            if(res.ok){
                res.json().then(id => console.log("City deleted with id " + id));
                this.loadData();
            }
            else
                res.json().then(err => alert("Failed to delete city: " + err.message));
        }).catch(err => alert("Error in sending data to server: " + err.message));
    }

    render() {
        return (
            <div >
                <p className="addcity"><a id="open" className="btn btn-success" onClick={(e) => this.toggle(e)}>Add a city?</a></p>
                <Modal isOpen={this.state.isOpen} toggle={this.toggle}>
                    <div className="col-md-3"></div>
                    <form onSubmit={(e) => this.addCity(e)} className="form-group col-md-6">
                        <h3>Add a city</h3>
                        <input id="cityName" className="form-control" name="cityName" type="text" placeholder="Name"/>

                        <input id="cityLongitude" className="form-control" name="cityLongitude" type="text" placeholder="Longitude"/>

                        <input id="cityLatitude" className="form-control" name="cityLatitude" type="text" placeholder="Latitude"/>

                        <input id="cityPicture" type="hidden" value=""/>
                        <ImagesUploader
                            label="Upload a Picture for the City"
                            url={HTTP_SERVER_PORT_IMAGES} optimisticPreviews multiple={false}
                            onLoadEnd={(err, pictureFileName) => {
                            if(err) console.error(err);
                            else document.getElementById("cityPicture").value = pictureFileName;
                            }}
                        />

                        <button type="submit" className="btn btn-success btn-lg">Create</button> <button className="btn btn-danger btn-lg" onClick={(e) => this.toggle(e)}>Cancel</button>
                    </form>
                    <div className="col-md-3"></div>
                </Modal>
                <div className="row">
                {this.state.cities.map((c,i) => <CityLaconic   city={c} key={i}/> )}
                </div>
            </div>
        );
    }
}

class CityLaconic extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props.city);
    }

    render() {
        return (
                    <div className="cardcity col-md-3 listCity">
                        <button onClick={(e) => this.deleteCity(e)}>&times;</button>
                        <Link to={'/city/' + this.props.city._id} className="">

                        <img className="card-img-top2" src={this.props.city.picture}/>
                        <div className="card-block">
                            <h5 className="text-bold">{this.props.city.name}</h5>
                        </div>
                        </Link>

                    </div>

        );
    }
}

