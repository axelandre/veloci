import express from 'express';
import corsPrefetch from 'cors-prefetch-middleware';
import imagesUpload from 'images-upload-middleware';
import { MongoClient, ObjectID } from 'mongodb';
import bodyParser from 'body-parser';

import { SERVER, PORT, HTTP_SERVER_PORT, HTTP_SERVER_PORT_IMAGES, IMAGES } from './constants';


const app = express();
app.use(express.static(__dirname + '/../static'));
app.use(bodyParser.json());
app.use(corsPrefetch);

let db; // global variable for getting an access to the database
MongoClient.connect('mongodb://' + SERVER)
    .then(connection => {
        db = connection.db('dbCities');
        app.listen(PORT, () => console.log('Server Listening on Port 9090'));
    })
    .catch(error => console.log('ERROR:', error));

app.get('/cities', (req, res) => {
    db.collection('cities').find().toArray() // Ask to the db object to find all cities
        .then(cities => res.json(cities))        // Transform in json and send them on the network
        .catch(error => {                        // Bad news : an error!
            console.log(error);
            res.status(500).json({ message: `Internal Server Error : ${error}` });
        });
});
app.post('/images', imagesUpload(
    './static/' + IMAGES,
    HTTP_SERVER_PORT_IMAGES
));

app.get('/cities/:id', (req, res) => {
    console.log("REQ1 ", req.params.id);
    db.collection('cities').findOne({"_id": ObjectID(req.params.id)})
        .then(city => res.json(city))
        .catch(error => {
            console.log(error);
            res.status(404).json({message: 'No such city with id : ${req.params.id}'});
        });

    app.get('/activities/:id', (req, res) => {
        console.log("REQ2 ", req.params.id);
        db.collection('activities').findOne({"_id": ObjectID(req.params.id)})
            .then(city => res.json(city))
            .catch(error => {
                console.log(error);
                res.status(404).json({message: 'No such city with id : ${req.params.id}'});
            });
    });
});

app.post('/newCity', (req, res) => {
    const c = {
        name: req.body.cityName,
        picture : req.body.cityPicture,
        coordinates: {
            lat: req.body.cityLatitude,
            long: req.body.cityLongitude
        },
        description: "",
        activities: []
    };
    db.collection('cities').insertOne(c)
        .then(result => res.json(result.insertedId))
        .catch(error => {
            console.log(error);
            res.status(500).json({message: `Internal Server Error: ${error}`});
        });
});

app.delete('/deleteCity', (req, res) => {
    const c = {
        name: req.body.cityName,
        picture : req.body.cityPicture,
        coordinates: {
            lat: req.body.cityLatitude,
            long: req.body.cityLongitude
        },
        description: "",
        activities: []
    };
    db.collection('cities').deleteOne(c)
        .then(result => res.json(result.insertedId))
        .catch(error => {
            console.log(error);
            res.status(500).json({message: `Internal Server Error: ${error}`});
        });
});

app.post('/newActivity', (req, res) => {
    const c = {
        name: req.body.activityName,
        nature: req.body.activityNature,
        editor: req.body.activityEditor,
        pictures : [req.body.activityPicture],
        comments: [],
        likers: "",
        description: req.body.activityDescription,
        url: ""
    };

    db.collection('activities').insertOne(c)
        .then(result =>
        {
            console.log("idCity", req.body.idCity);
            db.collection("cities").updateOne({
                "_id": ObjectID(req.body.idCity)
            }, {
                $push: {
                    "activities": {
                        _id: result.insertedId,
                        name: c.name,
                        nature: c.nature,
                        editor: c.editor,
                        picture: c.pictures[0],
                        comment: c.comment,
                        description: c.description
                    }
                }
            });
            console.log("insert", result.insertedId)
            res.json(result.insertedId);
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({message: `Internal Server Error: ${error}`});
        });
});

app.post('/newComment', (req, res) => {
    const c = {
        user: "",
        date: "",
        text: req.body.text
    };
console.log(db.collection('activities').findOne({
    "_id" : ObjectID(req.body.idActivity)
}));
    db.collection('activities').updateOne({
        "_id" : ObjectID(req.body.idActivity)
            }, {
                $push: {
                    "comments": {
                        user: c.user[0],
                        date: c.date,
                        text: c.text
                    }
                }
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({message: `Internal Server Error: ${error}`});
        });
});

